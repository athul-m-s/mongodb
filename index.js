const mongoose = require("mongoose");

//connecting to mongodb
mongoose
  .connect("mongodb://localhost/playground", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("connected to mongodb"))
  .catch((err) => console.log("error in connection with mongodb", err));

//schema creating : string number date buffer bool objectID array

const courseSchema = new mongoose.Schema({
  // name: String,
  // author: String,
  // tags: [String],
  // date: { type: Date, default: Date.now }, //initializing date at creation time
  // isPublished: Boolean,

  //with validation

  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 10,
    // match: /pattern/,
  },
  category: {
    type: String,
    required: true,
    enum: ["web", "mob", "network"], // validator , only these values accepted
    // lowercase: true,
    // uppercase: true,
    // trim: true,
  },
  author: String,
  tags: {
    type: Array,
    validate: {
      //async
      isAsync: true,
      validator: function (v, callback) {
        //do async fn
        setTimeout(() => {
          const result = v && v.length > 0;
          callback(result);
        }, 1000);
      },

      //not async //if length > 0 it is valid , return true is valid
      // validator: function (v) {
      //   return v && v.length > 0;
      // },
      message: "atleast one tag required", //if no tag passes to db it shows error
    },
  },
  date: { type: Date, default: Date.now }, //initializing date at creation time
  isPublished: Boolean,
  price: {
    type: String,
    min: 10,
    max: 100,
    get: (v) => {
      Math.round(v);
    },
    set: (v) => {
      Math.round(v);
    },
    required: function () {
      //if published true price is must value
      return this.isPublished;
    },
  },
});

//model :it creating a class
const Course = mongoose.model("Course", courseSchema);

async function createCourse() {
  //object create
  const course = new Course({
    name: "gitproject",
    category: "MOB",
    author: "mosh",
    tags: ["something"],
    //date dwfault value
    isPublished: true,
    price: 10,
  });

  //saving to db
  try {
    //course can be validate by

    //await course.validate(); //error goes to catch , it wont return value or anything

    const result = await course.save();
    console.log(result);
  } catch (ex) {
    // ex.error.tags
    // ex.error.name
    for (f in ex.errors) {
      console.log(ex.errors[f].message);
    }
  }
}

createCourse();

//querying to retrieve doc from db
async function getCourse() {
  // eq equal
  // ne not equal
  // gt greater than
  // gte greater than or equal to
  // lt less than
  // lte less than or equal to
  // in  in
  // nin not in

  //or
  //and

  //pagination
  // const pageNumber = 2;
  // const pageSize = 10;

  //eg : /api/courses?pageNumber=27pageSize=10

  return await Course.find();

  //pagination start
  // .find({ author: "Mosh", isPublished: true })
  // .skip((pageNumber - 1) * pageSize)
  // .limit(pageSize)
  //pagination end

  // exercise

  // .find({
  //   isPublished: true,
  //   tags: { $in: ["front-end", "backend"] },
  // })
  //   .sort({ name: 1 })
  //   .select({ name: 1, author: 1 });

  // .find({
  //     isPublished: true,
  //   })
  //     .or([{ tags: "front-end" }, { tags: "backend" }])
  //     .sort({ name: 1 })
  //     .select({ name: 1, author: 1 });

  // .find({
  //   isPublished: true,
  //   tags: "backend",
  // })
  //   .sort({ name: 1 })
  //   .select({ name: 1, author: 1 });

  // .find().or([
  //   { price: { $gte: 15 } },
  //   { name: /.*node.*/i },
  // ]);

  //count number of retrieved items
  // .find({ author: "mosh" }).countDocuments();
  //have mosh
  // .find({ author: /.*mosh.*/i });
  // case insensitive
  // .find({ author: /hamedani$/i });
  //ends with
  // .find({ author: /hamedani$/ });
  //starts with mosh
  // .find({ author: /^Mo/ });
  //working with tags
  // .find({ tags: "backend" })
  // .find({ price: { $gte: 10, $lte: 20 } })
  // .find({ price: { $gte: 10 } })
  // .find({price: {$in : [10,15,20]}})

  // .and([{ name: "Angular JS" }, { author: "Mosh" }]);
  // .or([{ author: "Moshamedani" }, { isPublished: true }]);
  // .find({ author: "Mosh" })
  // .limit(10)
  // .sort({ name: 1 }) //1 for ascendinf -1 for decending or .sort('name') or ('-name')
  // .select({ name: 1, tags: 1 }); // only show these fields or select('name author)
  // console.log(courses);
}

async function run() {
  const courses = await getCourse();
  console.log(courses);
}

// run();

//update course

async function updatedCourse(id) {
  //query first : find by id , update prop , save

  // const course = await Course.findById(id);
  // if (!course) return;
  // one
  // course.isPublished = false;
  // course.author = "athul";
  // two
  // course.set({
  //   isPublished: true,
  //   author: "athul",
  // });

  // const result = await course.save();

  // ================================================================

  //update first : update directly, optionally get updated document

  // const course = await Course.update({ isPublished: false }); //multiple data in one go

  //updateone wont give data in result
  // const result = await Course.updateOne(
  //   { _id: id },
  //   {
  //     $set: {
  //       author: "moshamedani",
  //       isPublished: true,
  //     },
  //   }
  // );

  const result = await Course.findByIdAndUpdate(
    id,
    {
      $set: {
        author: "json",
        isPublished: true,
      },
    },
    { new: true }
  );

  console.log(result);
}

// updatedCourse("6108254101b7683408f82f5d");

//remove or delete a document

async function removeCourse(id) {
  // const result = await Course.deleteOne({ _id: id }); //same & not get deleted data
  // const result = await Course.deleteOne({ _id: id });
  // const result = await Course.deleteMany({ _id: id });
  const result = await Course.findByIdAndDelete(id);
  console.log(result);
}

// removeCourse("6108254101b7683408f82f5d");
